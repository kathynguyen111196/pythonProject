import argparse
from functools import lru_cache


class WaterTowerSolution:
    def __init__(self, poured, row, glass):
        """
        :param poured: total number of glasses of water
        :param row: row number with top row 0
        :param glass: glass number within row with 1st glass 0
        """
        self.poured = poured
        self.row = row
        self.glass = glass

    @lru_cache(None)
    def helper(self, n):
        return (n * (n + 1)) // 2

    def get_left_right_node(self, index):
        N = (pow(1 + 8 * index, 0.5) - 1) // 2
        start_index = self.helper(int(N) + 1)
        offset = index - self.helper(int(N))
        return start_index + offset, start_index + offset + 1

    def water_tower(self) -> float:
        N = self.helper(self.row) + self.glass + 1
        cups = [0 for i in range(N)]
        cups[0] = float(self.poured)  # all water will go through 1st glass

        for i in range(N):
            if cups[i] <= 1:
                continue
            to_be_shared = cups[i] - 1
            cups[i] = 1

            left, right = self.get_left_right_node(i)
            if left < N:
                cups[left] += to_be_shared / 2
            if right < N:
                cups[right] += to_be_shared / 2

        return min(1.0, cups[self.helper(self.row) + self.glass])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('poured',
                        help="Enter total number of glasses of water.",
                        type=int)
    parser.add_argument('row',
                        help="Enter row number with top row 0.",
                        type=int)
    parser.add_argument('glass',
                        help="Enter glass number within row with 1st glass 0.",
                        type=int)
    args = parser.parse_args()
    water_tower = WaterTowerSolution(args.poured, args.row, args.glass)
    tmp = water_tower.water_tower()
    print('Result: {}'.format(tmp))
